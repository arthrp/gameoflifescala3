package ConsoleHelper;

def ANSI_CLS = "\u001b[2J";
def ANSI_HOME = "\u001b[H";

def clearScreen(): Unit = {
  System.out.print(ANSI_CLS + ANSI_HOME);
  System.out.flush();
}