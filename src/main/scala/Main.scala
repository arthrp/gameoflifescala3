import java.lang.Thread;
import java.lang.StringBuilder;
import ConsoleHelper._;

@main def hello: Unit = {
  val width = 20;
  val height = 20;
  var board = Array.ofDim[Boolean](height,width);

  initBoard(board);

  while (true) {
    printBoard(board,height,width);
    board = makeTurn(board);
    Thread.sleep(300);
  }
}

def initBoard(board: Array[Array[Boolean]]): Unit = {
  //Blinker
  board(2)(2) = true;
  board(2)(3) = true;
  board(2)(4) = true;
}

def printBoard(arr: Array[Array[Boolean]], maxHeight: Int, maxWidth: Int): Unit = {
  clearScreen();
  val s = new StringBuilder();
  for(w <- 0 to maxWidth-1) {
    for(h <- 0 to maxHeight-1) {
      s.append(if arr(w)(h) then "■" else "□");
    }
    s.append("\n");
  }
  println(s.toString());
}

def countNeighbours(board: Array[Array[Boolean]], cellX: Int, cellY: Int): Int = {
  var count = 0;
  val height = board.length;
  val width = board(0).length;

  for(deltaY <- -1 to 1) {
    var y = (cellY + deltaY + height) % height;
    for(deltaX <- -1 to 1) {
      var x = (cellX + deltaX + width) % width;
      count += (if board(x)(y) then 1 else 0);
    }
  }

  return (if board(cellX)(cellY) then count - 1 else count);
}

def getNewValue(board: Array[Array[Boolean]], x: Int, y: Int): Boolean = {
  val n = countNeighbours(board, x, y);
  val currentCell = board(x)(y);

  //Alive if is currently alive and 2 or 3 neighbours or if dead and 3 neighbours otherwise dead
  return (currentCell && (n == 2 || n == 3)) || (!currentCell && n == 3);
}

def makeTurn(board: Array[Array[Boolean]]): Array[Array[Boolean]] = {
  val height = board.length;
  val width = board(0).length;

  val newBoard = Array.ofDim[Boolean](width,height);
  for(h <- 0 to height-1) {
    for(w <- 0 to width-1) {
      newBoard(w)(h) = getNewValue(board,w,h);
    }
  }

  return newBoard;
}